<?php

/**
 * Returns list of modules to be installed.
 */
function aten_standard_profile_modules() {

  return array(
    'block',
    'comment',
    'contextual',
    'dashboard',
    'image',
    'list',
    'menu',
    'number',
    'options',
    'path',
    'taxonomy',
    'dblog',
    'search',
    'shortcut',
    'toolbar',
    'field_ui',
    'file',
    'ctools',
    'devel',
    'search_krumo',
    'views',
    'views_ui',
    'context',
    'context_ui',
    'menu_block',
    'token',
    'pathauto',
    'module_filter',
    'environment_indicator',
  );

} // aten_standard_profile_modules
